# Simple HTML Template with CSS

A simple website template using HTML and CSS. Feel free to use for your own website, as a base, but mention the original author.


## Installation
Copy all files into your website's directory. 

## Support
Feel free to report issues and ideas or ask questions, using the issue tracker.

## Contributing
No further development is done on this template. It is just a base for an own website. Feel free to improve or change it or add
functionality. 

## Authors and acknowledgment
Benjamin Buske 

## License
GNU Public License 2.0 (GPL)

## Project status
This template is provided as is. No further development is planned, except to fix bugs or urgent security issues. 